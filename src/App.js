
import {JsonCarsOjb, checkCar } from './info.js'


function App() {
  return (
    <div >
      <ul>
        {
          JsonCarsOjb.map((element, index) => {
            return <li key={index}>{element.make}/{element.vID}/ {checkCar(element.year)}</li>
          })
        }
      </ul>
    </div>
  );
}

export default App;
